#!/usr/bin/env python3

from jinja2 import Template
import json

# Load the Jinja2 template
with open('templates/school.jinja') as f:
    schoolTemplate = Template(f.read())

with open('templates/base.jinja') as f:
    baseTemplate = Template(f.read())

# Load the JSON data
with open('templates/data.json') as f:
    data = json.load(f)

# Render the template for schools in the data
for school in data['schools']:
    with open('ldifs/%s.ldif' % school['o'], 'w') as f:
        f.write(schoolTemplate.render(school=school))

# Render the template for schools in the data
with open('ldifs/base.ldif', 'w') as f:
    f.write(baseTemplate.render(base=data["base"]))