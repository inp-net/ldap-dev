# This dockerfile aim to export schema
FROM bitnami/openldap:2.6.6

USER root

RUN apt-get update && apt-get install -y python3 python3-jinja2

# Copy templates to the container
COPY templates /templates
COPY apply_template.py /apply_template.py

# Generate the schema files
RUN mkdir /ldifs
RUN python3 /apply_template.py

# Copy the schema files to the container
COPY schemas/qmail.schema /opt/bitnami/openldap/etc/schema/qmail.schema
COPY schemas/inp-net.schema /opt/bitnami/openldap/etc/schema/inp-net.schema

# Copy the export script to the container and make it executable
COPY  schemas/schema_conf.conf /schema_conf.conf

RUN mkdir /tmp/schemas

RUN slaptest -f /schema_conf.conf -F /tmp/schemas

RUN mkdir /schemas

RUN cp /tmp/schemas/cn\=config/cn\=schema/cn\=\{4\}qmail.ldif /schemas/qmail.ldif
RUN cp /tmp/schemas/cn\=config/cn\=schema/cn\=\{5\}inp-net.ldif /schemas/inp-net.ldif

RUN sed -i 's/{.}qmail/qmail/g' /schemas/qmail.ldif
RUN sed -i 's/dn: cn=qmail/dn: cn=qmail,cn=schema,cn=config/g' /schemas/qmail.ldif

RUN sed -i 's/{.}inp-net/inp-net/g' /schemas/inp-net.ldif
RUN sed -i 's/dn: cn=inp-net/dn: cn=inp-net,cn=schema,cn=config/g' /schemas/inp-net.ldif

RUN sed -i '$d' /schemas/inp-net.ldif
RUN sed -i '$d' /schemas/inp-net.ldif
RUN sed -i '$d' /schemas/inp-net.ldif
RUN sed -i '$d' /schemas/inp-net.ldif
RUN sed -i '$d' /schemas/inp-net.ldif
RUN sed -i '$d' /schemas/inp-net.ldif

RUN sed -i '$d' /schemas/qmail.ldif
RUN sed -i '$d' /schemas/qmail.ldif
RUN sed -i '$d' /schemas/qmail.ldif
RUN sed -i '$d' /schemas/qmail.ldif
RUN sed -i '$d' /schemas/qmail.ldif
RUN sed -i '$d' /schemas/qmail.ldif

RUN chown -R 1001:0 /schemas

USER 1001