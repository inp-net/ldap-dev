# Ldap-Dev

This repository aim to create a clone of inp-net ldap structure with fake data.

# Getting started
```
docker compose up -d
```

Le compte manager est cn=admin,dc=etu-inpt,dc=fr:admin

Les autres utilisateurs ont pour mot de passe leur uid

# Contribute

If you want to contribute, juste fork the repo and create a MR.